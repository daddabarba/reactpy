from abc import ABC
from typing import Any, Callable, Generic, TypeVar, ParamSpec

from lockattrs import protect

# Generic wild card for operator's output
RET_T = TypeVar("RET_T")
IN_T = ParamSpec("IN_T")


class OperatorWrapper(ABC, Generic[IN_T, RET_T]):
    """
    General wrapper for function. This decorator functionally behaves the same as the function passed. It is an abstract class at objects of this type should not be instantiated, its only purpose is to have a share super-class for the operator types
    """

    __slots__ = ("__f",)

    # CONSTRUCTION

    def __init__(self, f: Callable[IN_T, RET_T], /):

        # Store function to be called
        self.__f: Callable[IN_T, RET_T] = f

    # CALLABLE BEHAVIOUR

    def __call__(self, *args: IN_T.args, **kwargs: IN_T.kwargs) -> RET_T:
        """
        Method to make operator wrapper behave as the function stored in the `__f` field
        """

        # Forward arguments to inner function
        return self.__f(*args, **kwargs)

    # GETTERS

    @property
    def f(self) -> Callable[IN_T, RET_T]:
        return self.__f

    # IMMUTABILITY

    @protect(("_operator__f"))
    def __setattr__(self, name: str, val: Any) -> None:
        """
        This method protects the field `__f` from being set more than once (thus prevents it to be overwritten after construction). Combined with the __slots__ definition, this class is immutable
        """
        super(OperatorWrapper, self).__setattr__(name, val)
