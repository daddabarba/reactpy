from reactpy.core.pipedec import OperatorWrapper


class filter_pipe(OperatorWrapper):
    """
    This wrapper behaves the same as the function it decorates, with the added benefit of being able to use `isinstance` to indicate that the given function specifies a filter and not an operator
    """

    pass


class macro_pipe(OperatorWrapper):
    """
    This wrapper behaves the same as the function it decorates, with the added benefit of being able to use `isinstance` to indicate that the given function specifies a filter and not an operator
    """

    pass


class observer(OperatorWrapper):
    """
    This wrapper behaves the same as the function it decorates, with the added benefit of being able to use `isinstance` to indicate tht the wrapped function needs to be subscribed as observer to a pipe
    """

    pass
