from dataclasses import dataclass
from enum import Enum

from rx.core.typing import Observer, Scheduler
from typing import List, Optional


class Vals(Enum):
    """
    Basic types to define values
    """

    empty = 0  # represents a value being empty (for cases where None may be an actual value and not empty)


class RxTuple(tuple):
    def __getitem__(self, slice):
        return RxTuple(super(RxTuple, self).__getitem__(slice))

    def __str__(self):
        return f"RxTuple{super(RxTuple, self).__str__()}"


@dataclass
class ObserversStruct:
    """
    Dataclass to store observers and schedules (for specific node)
    """

    observers: List[Observer]
    schedulers: List[Scheduler]

    def register(self, observer: Observer, scheduler: Optional[Scheduler] = None):
        """
        Define function for Observable (register observer)
        """
        self.observers.append(observer)
        self.schedulers.append(scheduler)
