from typing import Callable, Optional

from reactpy.core.base_types import RxTuple
from rx import operators as op
from rx.core import Observable as ObservableBase
from rx.core.typing import Mapper, Observable, Predicate
from rx.disposable import CompositeDisposable, SingleAssignmentDisposable
from rx.internal.utils import NotSet


def __unpacker(f: Callable):
    return lambda x: f(x) if not isinstance(x, RxTuple) else f(*x)


def unpack_map(mapper: Optional[Mapper] = None) -> Callable[[Observable], Observable]:

    if mapper is None:
        return op.map()

    return op.map(__unpacker(mapper))


def unpack_filter(predicate: Predicate) -> Callable[[Observable], Observable]:
    return op.filter(__unpacker(predicate))


# PIPING OPERATORS


def _with_latest_from(parent: ObservableBase, *sources: Observable) -> Observable:
    """
    Modified from https://github.com/ReactiveX/RxPY/blob/f2642a87e6b4d5b3dc9a482323716cf15a6ef570/rx/core/observable/withlatestfrom.py
    """
    NO_VALUE = NotSet()

    def subscribe(observer, scheduler=None):
        def subscribe_all(parent, *children):

            values = [-1 if parent is not None else 1] + [NO_VALUE for _ in children]

            def subscribe_child(i, child):
                subscription = SingleAssignmentDisposable()

                def on_next(value):

                    if parent is not None:
                        with parent.lock:
                            values[i + 1] = value
                    else:
                        values[i + 1] = value

                    # Check parent's value. If not None, then call on_next
                    if NO_VALUE not in values and values[0] >= 0:
                        observer.on_next(tuple(values))

                subscription.disposable = child.subscribe_(
                    on_next, observer.on_error, scheduler=scheduler
                )
                return subscription

            parent_subscription = SingleAssignmentDisposable()

            if parent is not None:

                def on_next(value):
                    with parent.lock:

                        # Update value of parent
                        values[0] = value

                        # If all other values are set, and flag is set to 1
                        if NO_VALUE not in values and value == 1:
                            observer.on_next(tuple(values))

                disp = parent.subscribe_(
                    on_next, observer.on_error, observer.on_completed, scheduler
                )
                parent_subscription.disposable = disp

            children_subscription = [
                subscribe_child(i, child) for i, child in enumerate(children)
            ]

            return (
                [parent_subscription] if parent is not None else []
            ) + children_subscription

        return CompositeDisposable(subscribe_all(parent, *sources))

    return ObservableBase(subscribe)


def with_latest_from(*sources: Observable) -> Callable[[Observable], Observable]:
    """
    Modified from https://github.com/ReactiveX/RxPY/blob/f2642a87e6b4d5b3dc9a482323716cf15a6ef570/rx/core/operators/withlatestfrom.py
    """

    def with_latest_from(source: Observable) -> Observable:
        return _with_latest_from(source, *sources)

    return with_latest_from
