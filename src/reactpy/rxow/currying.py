from functools import partial
from inspect import _ParameterKind, signature
from typing import Any, Callable, Dict, Generic, Iterator, ParamSpec, Tuple, TypeVar

from reactpy.core.base_types import Vals
from reactpy.core.pipedec import OperatorWrapper

# Generic wild card for function's output
RET_T = TypeVar("RET_T")
# Generic wild card for function's input
IN_T = ParamSpec("IN_T")


class cur(partial, Generic[IN_T, RET_T]):
    """
    This wrapper allows to perform position based and key-value based currying on a given callable. It is also customized to account for decorators from the reactpy library and avoid to offuscate them (that is, if currying is applied on a function decorated by a decorator belonign to a certain class family, the decorator is kept outside of the result of currying). The class effectively acts as a callable as if the given positional/keyword arguments were not declared as inputs of the given callable
    """

    # ATTRIBUTES

    _exposed: Tuple  # tuple of arguments left to give as input (original arguments without the given positiona/keywords curs)

    # CONSTRUCTION

    def __new__(
        cls, func: Callable[IN_T, RET_T], /, *args: IN_T.args, **keywords: IN_T.kwargs
    ) -> "cur":

        # Init object
        self = super(cur, cls).__new__(
            cls,
            func if not isinstance(func, OperatorWrapper) else func.f,
            *args,
            **keywords,
        )

        # Save exposed function arguments. This list serves to blend keyword arguments with keyword curs. For each given argument a tuple with its name and value (empty if exposed/to be given) is stored

        _exposed = []
        _var_args_passed = False  # set to true after encountering var-args

        for arg_pos, arg in enumerate(signature(func).parameters.values()):

            # Skip arguments current by position
            if arg_pos < len(args):
                continue

            # Start with empty value
            arg_val = Vals.empty

            # Fill in with positional type of keyword type if possible
            if arg.kind is _ParameterKind.VAR_POSITIONAL:
                arg_val = arg.kind
                _var_args_passed = True

            # Fill in value if given (after passing variable arguments this does not hold anymore)
            if arg.name in keywords.keys() and not _var_args_passed:
                arg_val = self.keywords.pop(arg.name)

            # Append argument
            _exposed.append((arg.name, arg_val))

        self._exposed = tuple(_exposed)

        return self if not isinstance(func, OperatorWrapper) else type(func)(self)

    # FUNCTION BEHAVIOR

    def __call__(self, /, *args: IN_T.args, **keywords: IN_T.kwargs) -> RET_T:
        """
        This method makes an object of type cur behave as a function passed, it takes the given arguments and completes them with the curred arguments stored
        """

        args_iter = iter(args)
        return self.func(
            *self.args,  # arguments curred by position have to be the first N
            *self.__place_args(
                args_iter, keywords
            ),  # fill in exposed arguments with given keyword arguments, use positional if not in keywords
            *args_iter,  # remaining arguments need to be passed by position (if curring happens in the middle)
            **keywords,  # unpack the remaining keyword arguments that have not been used to fill in the blanks in exposed yet
            **self.keywords,  # unpack arguments curred by keyword
        )

    # AUXILLIARY METHOD

    def __place_args(self, args_iter: Iterator, keywords: Dict[str, Any]) -> None:
        """
        This method creates positional arguments to be passed, replacing empty slots with var args (psotional if possible, otherwise keyword)
        """

        stop_pos = False

        for name, v in self.exposed:

            # If current argument is var_args, keep dumping positional arguments until finished
            if v is _ParameterKind.VAR_POSITIONAL:
                for x in args_iter:
                    yield x
                stop_pos = True
                continue

            # Use value from keyword currying if given
            if v is not Vals.empty:
                yield v
                continue

            # If buffer of positional arguments is not empty, pop from buffer
            if not stop_pos:
                try:
                    yield next(args_iter)
                    continue
                except StopIteration:
                    stop_pos = True

            # If value has been passed through keyword, use that
            if name in keywords:
                yield keywords.pop(name)

    # Getters

    @property
    def exposed(self) -> Tuple[Tuple[str, Any]]:
        return self._exposed
