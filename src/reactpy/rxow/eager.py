from collections import OrderedDict
from inspect import getargs
from warnings import warn

from reactpy.core.decorators import macro_pipe
from reactpy.rxow.vario import elicited, variable


def compile(macro: macro_pipe):
    """
    This function generates a python callable equivalent to calling the macro a standard python function. This callable will have internal varialbes that are set when an input is given, and the elicited value is returned
    """

    # Get signature of inner function
    s = getargs(macro.f.__code__)

    # Warning in case of unspported var args (since this would require to recreate the pipe every time the function is run)
    if s.varargs is not None:
        warn("varargs currently not supported for compilation")

    if s.varkw is not None:
        warn("kwargs currently not supported for compilation")

    # Create (ordered) dictionary of variables to be set when input is given
    inputs = OrderedDict()

    for arg_name in s.args:

        # Add to dict
        inputs[arg_name] = variable()

    # Create pipe to run macro eagerly
    inputs_l = list(inputs.values())
    pipe = sum(inputs_l[1:], inputs_l[0]) | macro

    # Create elicited object to catch the output
    out = elicited()
    pipe.subscribe(**out)

    # Build function to be run eagerly
    def _f(*args, **kwargs):

        # Set positional arguments
        for var, val in zip(list(inputs.values())[: len(args)], args):
            var.set(val)

        # Set keyword arguments
        for k, val in kwargs.items():
            inputs[k].set(val)

        # Return output of elicited
        return out.val

    # Return eager function
    return _f
