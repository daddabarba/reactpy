from typing import Any, Generic, List, Tuple, TypeVar

from lockattrs import protect
from reactpy.core.base_types import ObserversStruct, Vals
from reactpy.core.decorators import observer
from reactpy.rxow.rxwrapper import ObservableWrapper
from rx import create
from rx.core.typing import Observer

T_var = TypeVar("T_var")


class variable(ObservableWrapper[T_var]):
    """
    This class can be used as a source for an RxPy stream. Value can be passed one by one and it will keep notifying it's observers
    """

    # ATTRIBUTES

    _observers: ObserversStruct

    # CONSTRUCTION

    def __init__(self):
        pass

    def __new__(cls) -> "variable":

        # Create dataclass to store observers (since object `variable` does not exist at this point yet)
        struct = ObserversStruct([], [])

        # Create Observable
        observable = create(struct.register)

        # Cast observable to `variable` class
        observable.__class__ = cls  # TODO using __slots__ with casting

        # Add attribute for observers of this variable
        setattr(observable, f"_observers", struct)

        return observable

    # STREAM HANDLING

    def set(self, *vals: T_var) -> "variable":
        """
        This method can be called to send a value upstream from this source
        """

        # Send value up stream
        for val in vals:
            for observer in self.observers:
                observer.on_next(val)

        # Return self to concatenate operations
        return self

    def close(self) -> "variable":
        """
        This method can be called to send `on_completed` (to all the observers) from this source
        """

        # Signal end of stream
        for observer in self.observers:
            observer.on_completed()

        # Return self to concatenate operations
        return self

    # `WITH` NOTATION

    def __enter__(self) -> "variable":
        """
        Allow to use variable in with statement, closing stream at the end
        """
        return self

    def __exit__(self, *args, **kwargs) -> None:
        """
        Allow to use variable in with statement, closing stream at the end
        """
        self.close()

    # GETTERS

    @property
    def obs(self) -> observer:
        return observer(self.set)

    @property
    def observers(self) -> List[Observer]:
        return self._observers.observers

    # IMMUTABLE
    # TODO: find a better way to do this (with metaprogramming perhaps)
    @protect(("_observers",))
    def __setattr__(self, name: str, value: Any) -> None:
        super(variable, self).__setattr__(name, value)


T_el = TypeVar("T_el")


class elicited(Generic[T_el]):

    # ATTRIBUTES

    __slots__ = ("_val", "_done")

    _val: T_el
    _done: bool

    def __init__(self):

        self._val = Vals.empty
        self._done = False

    # OBSERVER EVENTS

    def on_next(self, x: T_el):
        """
        Observer to be passed for on_next event (stores last value)
        """

        self._val = x
        self._done = False

    def on_completed(self):
        """
        Observer to be passed for on_next event (stores boolean if completed)
        """

        self._done = True

    # GETTERS

    @property
    def val(self) -> T_el:
        return self._val

    @property
    def done(self) -> bool:
        return self._done

    def __getitem__(self, key: str) -> Any:
        return getattr(self, key)

    def keys(self) -> Tuple[str]:
        return ("on_next", "on_completed")
