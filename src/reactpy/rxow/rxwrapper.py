from abc import ABC, abstractmethod
from copy import copy
from typing import Any, Callable, Generic, Tuple, TypeVar

from lockattrs import protect
from reactpy.core.base_types import ObserversStruct, RxTuple
from reactpy.core.decorators import filter_pipe, macro_pipe, observer
from reactpy.core.operators import unpack_map, with_latest_from
from rx import create
from rx import operators as op
from rx.core.observable.observable import Observable as ObservableBase
from rx.core.typing import Observable

T = TypeVar("T")


class ObservableWrapper(ObservableBase, Generic[T]):
    """
    This class emulates an rx.core.observable.observable.Observable type object (by containing one), and adds extra functionality. This mainly involves piping and forking. The output of pipes is wrapped in this same class
    """

    # ATTRIBUTES

    _observers: ObserversStruct
    _output_var: Observable

    # CONSTRUCTION

    def __init__(self, observable: ObservableBase):
        pass

    def __new__(cls, observable: ObservableBase) -> "ObservableWrapper":

        # Copy object to not modify `__class__` attribute of original one
        observable = copy(observable)

        # Cast object
        observable.__class__ = cls  # TODO using __slots__ with casting

        return observable

    # PIPING

    def __or__(self, other: Callable[[T], Any]) -> "ObservableWrapper":
        """
        Method that allows to pipe an ObservableWrapper with a function. If nothing is specified (e.g. a callable of unkown type is passed), then rx.operator.map is used by default, otherwise, depending on the decorator used on the callable, a different operator will be selected
        """

        # TODO support for more operators?

        # Type checking
        if not isinstance(other, Callable):
            raise TypeError(f"Expected Callable type instead got {type(other)}")

        # If wrapper for filter was used, use rx.operators.filter
        if isinstance(other, filter_pipe):
            return ObservableWrapper(self.pipe(op.filter(other.f)))

        # If wrapper for macro is used, run macro at compilation time
        if isinstance(other, macro_pipe):
            return self.to_macro(other)

        # If wrapper for observer is used, add observer and keep piping
        if isinstance(other, observer):
            self.subscribe(on_next=other.f)
            return self

        # By default, use rx.operators.map
        return ObservableWrapper(self.pipe(unpack_map(other)))

    def to_macro(self, other: Callable[[T], Any]) -> "ObservableWrapper":
        """
        Function that forwards current observable as input to macro
        """

        return other(self)

    def __rshift__(self, other: Callable) -> "ObservableWrapper":
        """
        This method allows an ObservableWrapper object to be forked into multiple pipes (perhaps on different threads). The last function of the pipeline (the current ObservableWrapper) is turned into an Observer (that is, an observer is created that simply communicates the current pipeline's final value). Then a variable is used to push the aforementioned value upstream multiple pipes (forked from this point on). A single variable is created, on the first fork, then it is recycled in the other forks
        """

        if not isinstance(other, Callable):
            raise TypeError(f"Expected Callable type instead got {type(other)}")

        if not hasattr(self, "_output_var"):

            # Create variable
            self._observers = ObserversStruct([], [])

            # Create Observer
            self._output_var = ObservableWrapper(create(self._observers.register))

            # Create observable
            self.subscribe(
                on_next=lambda x: [
                    observer.on_next(x) for observer in self._observers.observers
                ],
                on_completed=lambda: [
                    observer.on_completed() for observer in self._observers.observers
                ],
            )

        # Pipe variable with next callable
        return self._output_var | other

    def __and__(self, other: Observable) -> "ObservableConjointWrapper":
        return ObservableConjointWrapper(self, other)

    def __add__(self, other: Observable) -> "ObservableDisjointWrapper":
        return ObservableDisjointWrapper(self, other)

    # GETTERS

    @property
    def observable(self) -> Observable:
        return self

    # IMMUTABLE

    @protect(("_output_var", "_observers"))
    def __setattr__(self, name: str, value: Any) -> None:
        super(ObservableWrapper, self).__setattr__(name, value)


class MultiObservableWrapper(ABC):

    # ATTRIBUTES

    __slots__ = ("_first", "_others")

    _first: Observable
    _others: Tuple[Observable]

    __methods_delegation = tuple(
        (
            name
            for name in dir(ObservableWrapper)
            if callable(getattr(ObservableWrapper, name))
            and name
            not in (
                "__init__",
                "__class__",
                "__getattribute__",
                "__setattr__",
                "to_macro",
            )
        )
    )

    # CONSTRUCTION

    def __init__(self, first: Observable, *others: Observable):

        self._first = first
        self._others = tuple(others)

    # PIPING

    # TODO support for more operators? e.g. join, zip, ...

    def __or__(self, other: Callable) -> "ObservableWrapper":
        """
        This method is needed otherwise the class is not recognized as allowing this operator (even though its function is delegated to ObservableWrapper)
        """

        if isinstance(other, macro_pipe):
            return self.to_macro(other)

        return self.joined | other

    def to_macro(self, other: Callable[[T], Any]) -> Observable:
        """
        Function that forwards current observable as input to macro
        """

        return other(self._first, *self._others)

    def __rshift__(self, other: Callable) -> "ObservableWrapper":
        """
        This method is needed otherwise the class is not recognized as allowing this operator (even though its function is delegated to ObservableWrapper)
        """

        return self.joined >> other

    # DELEGATE BEHAVIOUR TO OBSERVABLEWRAPPER

    def __getattribute__(self, name: str) -> Any:

        if name in ObservableConjointWrapper.__methods_delegation:
            return getattr(self.joined, name)

        return super(MultiObservableWrapper, self).__getattribute__(name)

    # GETTERS

    @property
    @abstractmethod
    def joined(self) -> "ObservableWrapper":
        pass

    # IMMUTABLE

    @protect(("_first", "_others"))
    def __setattr__(self, name: str, value: Any) -> None:
        super(MultiObservableWrapper, self).__setattr__(name, value)


class ObservableConjointWrapper(MultiObservableWrapper):
    def __and__(self, other: Observable) -> "ObservableConjointWrapper":
        return ObservableConjointWrapper(self._first, *self._others, other)

    @property
    def joined(self):
        return ObservableWrapper(
            self._first.pipe(op.zip(*self._others), op.map(lambda x: RxTuple(x)))
        )


class ObservableDisjointWrapper(MultiObservableWrapper):
    def __add__(self, other: Observable) -> "ObservableDisjointWrapper":
        return ObservableDisjointWrapper(self._first, *self._others, other)

    @property
    def joined(self):
        return ObservableWrapper(
            self._first.pipe(
                with_latest_from(*self._others), op.map(lambda x: RxTuple(x[1:]))
            )
        )
