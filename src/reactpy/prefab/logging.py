from datetime import datetime

from reactpy.core.base_types import RxTuple
from rich.console import Console


def log(*args: str, id=""):
    """
    Identity function with side effect of logging the object(s) given as input
    """
    print("ID: ", id)
    Console().print(f"[bold red]{id}[/bold red]","[bold](" + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ")[/bold]\n", *args)
    return RxTuple(args)
