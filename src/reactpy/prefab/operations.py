from functools import reduce
from typing import Callable

from reactpy.core.base_types import RxTuple
from reactpy.core.decorators import macro_pipe
from reactpy.core.operators import _with_latest_from
from reactpy.rxow.rxwrapper import ObservableWrapper
from rx import operators as op
from rx.core.typing import Observable


def anyp(first: ObservableWrapper, *others: ObservableWrapper):
    """
    Function that performs `+` operation (Disjunctive Concatenation) on a series of given Observables
    """

    if len(others) == 0:
        return first

    return reduce(lambda a, b: a + b, others, first)


def allp(first: ObservableWrapper, *others: ObservableWrapper):
    """
    Function that performs `+` operation (Conjunctive Concatenation) on a series of given Observables
    """

    if len(others) == 0:
        return first

    return reduce(lambda a, b: a & b, others, first)


def with_latest_from_flagless(
    *sources: Observable,
) -> Callable[[Observable], Observable]:
    """
    Modified from https://github.com/ReactiveX/RxPY/blob/f2642a87e6b4d5b3dc9a482323716cf15a6ef570/rx/core/operators/withlatestfrom.py
    """

    def with_latest_from(source: Observable) -> Observable:
        return _with_latest_from(None, source, *sources)

    return with_latest_from


@macro_pipe
def add_flag(fist: ObservableWrapper, *others: ObservableWrapper):
    """
    Creates a flag and sets it to true to have the values change every time one of the sources is updated
    """

    # Create pipe
    return ObservableWrapper(
        fist.pipe(with_latest_from_flagless(*others), op.map(lambda x: RxTuple(x[1:])))
    )
