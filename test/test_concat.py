from pytest import mark
from reactpy.core.base_types import Vals
from reactpy.prefab.operations import add_flag
from reactpy.rxow.vario import elicited, variable
from rx import operators as op


def add1(x):
    return x + 1


def mul2(x):
    return x * 2


def redsum(x):
    return sum(x)


def sum2(x, y):
    return x + y


def sum3(x, y, z):
    return x + y + z


@mark.piping
def test_simple_cat():

    # Create two variables
    source1 = variable()
    source2 = variable()

    # Create two pipes
    pipe1 = source1 | add1
    pipe2 = source2 | mul2

    # Concatenate pipes
    pipe_cat = pipe1 & pipe2

    # Create observer
    out = elicited()
    obs = pipe_cat.subscribe(**out)

    # Check output
    source1.set(2)
    source2.set(3)

    assert out.val == (3, 6) and not out.done


@mark.piping
def test_cat_with_followup():

    # Create two variables
    source1 = variable()
    source2 = variable()

    # Create two pipes
    pipe1 = source1 | add1
    pipe2 = source2 | mul2

    # Concatenate pipes
    pipe_cat = (pipe1 & pipe2).pipe(op.map(redsum))

    # Create observer
    out = elicited()
    obs = pipe_cat.subscribe(**out)

    # Check output
    source1.set(2)
    source2.set(3)

    assert out.val == 9 and not out.done


@mark.piping
def test_cat_with_piping():

    # Create two variables
    source1 = variable()
    source2 = variable()

    # Create two pipes
    pipe1 = source1 | add1
    pipe2 = source2 | mul2

    # Concatenate pipes
    pipe_cat = (pipe1 & pipe2) | sum2

    # Create observer
    out = elicited()
    obs = pipe_cat.subscribe(**out)

    # Check output
    source1.set(2)
    source2.set(3)

    assert out.val == 9 and not out.done


@mark.piping
def test_cat_with_three():

    # Create two variables
    source1 = variable()
    source2 = variable()
    source3 = variable()

    # Create two pipes
    pipe1 = source1 | add1
    pipe2 = source2 | mul2
    pipe3 = source3 | mul2

    # Concatenate pipes
    pipe_cat = (pipe1 & pipe2 & pipe3) | sum3

    # Create observer
    out = elicited()
    obs = pipe_cat.subscribe(**out)

    # Check output
    source1.set(2)
    source2.set(3)
    source3.set(4)

    assert out.val == 17 and not out.done


# TODO test multi input filtering


@mark.piping
def test_disjoint_cat():

    # Create two variables
    source1 = variable()
    source2 = variable()
    source3 = variable()

    flag = variable()

    # Create two pipes
    pipe1 = source1 | add1
    pipe2 = source2 | mul2
    pipe3 = source3 | mul2

    # Concatenate pipes
    pipe_cat = (flag + pipe1 + pipe2 + pipe3) | sum3

    # Create observer
    out = elicited()
    obs = pipe_cat.subscribe(**out)

    # Check output

    # Setting flag to update always
    flag.set(0)
    assert out.val is Vals.empty and not out.done

    # Value should remain unchanged since source2 and source3 are empty still
    source1.set(2)
    assert out.val is Vals.empty and not out.done

    # Value should be elicited when source 3 is set, as no empty values are found
    source2.set(3)
    source3.set(4)
    assert out.val == 17 and not out.done

    # Value should be elicited since there has been a change in one of the source
    source2.set(-2)
    assert out.val == 7 and not out.done

    # Value should be elicited since there has been a change in one of the source
    source1.set(3)
    assert out.val == 8 and not out.done

    # Setting flag to not update output
    flag.set(-1)
    assert out.val == 8 and not out.done

    # Value should not change since flag is -1
    source1.set(5)
    assert out.val == 8 and not out.done

    # Value should not change since flag is -1
    source2.set(-1)
    assert out.val == 8 and not out.done

    # Value should remain unchanged since flag 0 does not elicit on_next
    flag.set(0)
    assert out.val == 8 and not out.done

    # Setting the flag to 1 should also update the values
    flag.set(1)
    assert out.val == 12 and not out.done


@mark.piping
def test_disjoint_cat():

    # Create two variables
    source1 = variable()
    source2 = variable()
    source3 = variable()

    # Create two pipes
    pipe1 = source1 | add1
    pipe2 = source2 | mul2
    pipe3 = source3 | mul2

    # Concatenate pipes
    pipe_cat = (pipe1 + pipe2 + pipe3) | add_flag | sum3

    # Create observer
    out = elicited()
    obs = pipe_cat.subscribe(**out)

    # Check output

    # Value should remain unchanged since source2 and source3 are empty still
    source1.set(2)
    assert out.val is Vals.empty and not out.done

    # Value should be elicited when source 3 is set, as no empty values are found
    source2.set(3)
    source3.set(4)
    assert out.val == 17 and not out.done

    # Value should be elicited since there has been a change in one of the source
    source2.set(-2)
    assert out.val == 7 and not out.done

    # Value should be elicited since there has been a change in one of the source
    source1.set(3)
    assert out.val == 8 and not out.done
