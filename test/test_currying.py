from pytest import mark
from reactpy.rxow.currying import cur


# Define function with arity 5
def f_penta(a: int, b: int, c: int, d: int, e: int):
    return a, b, c, d, e


# Define function with arity 5
def f_penta_def(a: int, b: int, c: int, d: int, e: int = -1):
    return a, b, c, d, e


# Define function with positional var args
def f_var_pos(a: int, b: int, c: int, d: int, e: int, *args):
    return a, b, c, d, e, *args


# Define function with positional var args and default values
def f_var_pos_def(*args: int, v=1):
    return tuple([*args, v])


# Define function with positional var args preceeded by default values
def f_def_var_pos(v=1, *args: int):
    return tuple([*args, v])


# Define function with keyword arguments
def f_var_key(a: int, b: int, c: int, d: int, e: int, **kwargs):
    return a, b, c, d, e, kwargs


@mark.currying
def test_currying_simple():
    """
    Testing currying simple function
    """

    # Cur first 2 arguments by position and fourth by keyword
    f_penta_ce = cur(f_penta, 0, 1, d=3)

    assert f_penta_ce(2, 4) == tuple(range(5))
    assert f_penta_ce(e=4, c=2) == tuple(range(5))
    assert f_penta_ce(2, e=4) == tuple(range(5))


@mark.currying
def test_currying_default():
    """
    Testing curruing function with default values for some parameters
    """

    # Cur first 2 arguments by position and fourth by keyword
    f_penta_ce = cur(f_penta_def, 0, 1, d=3)

    assert f_penta_ce(2, 4) == tuple(range(5))
    assert f_penta_ce(e=4, c=2) == tuple(range(5))
    assert f_penta_ce(2, e=4) == tuple(range(5))
    assert f_penta_ce(2) == tuple(range(4)) + (-1,)
    assert f_penta_ce(c=2) == tuple(range(4)) + (-1,)


@mark.currying
def test_currying_pos_var():
    """
    Testing currying function with positional var args
    """

    # Cur first 2 arguments by position and fourth by keyword
    f_var_pos_cur = cur(f_var_pos, 0, 1, d=3)

    assert f_var_pos_cur(2, 4, 5, 6) == tuple(range(7))
    assert f_var_pos_cur(2, 4) == tuple(range(5))
    assert f_var_pos_cur(e=4, c=2) == tuple(range(5))
    assert f_var_pos_cur(2, e=4) == tuple(range(5))


@mark.currying
def test_currying_pos_var_def():
    """
    Testing currying function with positional var args and a variable with default value
    """

    # Cur variable with default value
    f_var_pos_def_cur = cur(f_var_pos_def, v=10)

    assert f_var_pos_def_cur(1, 2, 3) == (1, 2, 3, 10)
    assert f_var_pos_def_cur(1, 2) == (1, 2, 10)


@mark.currying
def test_currying_def_pos_var():
    """
    Testing currying function with positional var args and a variable with default value (before the positional var args)
    """

    # Cur variable with default value
    f_def_var_pos_cur = cur(f_def_var_pos, v=10)

    assert f_def_var_pos_cur(1, 2, 3) == (1, 2, 3, 10)
    assert f_def_var_pos_cur(1, 2) == (1, 2, 10)

    # Cur variable with default value (by position)
    f_def_var_pos_cur = cur(f_def_var_pos, 10)

    assert f_def_var_pos_cur(1, 2, 3) == (1, 2, 3, 10)
    assert f_def_var_pos_cur(1, 2) == (1, 2, 10)


@mark.currying
def test_currying_pos_key():
    """
    Testing currying function with keyword var arguments
    """

    # Cur first 2 arguments by position, fourth by keyword, and add extra default var arg `z`
    f_var_key_cur = cur(f_var_key, 0, 1, d=3, z=-1)

    assert f_var_key_cur(2, 4) == tuple(range(5)) + (dict(z=-1),)
    assert f_var_key_cur(e=4, c=2) == tuple(range(5)) + (dict(z=-1),)
    assert f_var_key_cur(2, e=4) == tuple(range(5)) + (dict(z=-1),)
    assert f_var_key_cur(2, 4, j=-2) == tuple(range(5)) + (dict(z=-1, j=-2),)
    assert f_var_key_cur(e=4, c=2, j=-2) == tuple(range(5)) + (dict(z=-1, j=-2),)
    assert f_var_key_cur(2, e=4, j=-2) == tuple(range(5)) + (dict(z=-1, j=-2),)
