from pytest import mark
from reactpy.core import decorators
from reactpy.prefab.operations import allp
from reactpy.rxow.currying import cur
from reactpy.rxow.eager import compile
from reactpy.rxow.rxwrapper import ObservableWrapper
from reactpy.rxow.vario import elicited, variable


def add(a: float, b: float):
    return a + b


def mul(a: float, b: float):
    return a * b


def power(a: float, b: int):
    return a ** b


def addall(*args: float):
    return sum(args)


@decorators.macro_pipe
def f(x: ObservableWrapper[float]):
    """
    This macro defines the operation of creating a function 2*x+1 for a given observable x
    """

    doubled = x | cur(mul, b=2)
    incremented = x | cur(add, b=1)

    return (doubled & incremented) | add


@decorators.macro_pipe
def g(x: ObservableWrapper[float], y: ObservableWrapper[float]):
    """
    This macro defines the operation of creating a function 2*x + (y+1)^2 for given observables x and y
    """

    doubled = x | cur(mul, b=2)
    squared = y | cur(add, b=1) | cur(power, b=2)

    return (doubled & squared) | add


@decorators.macro_pipe
def h(
    x: ObservableWrapper[float],
    *args: ObservableWrapper[float],
):

    x = x | cur(mul, b=2)
    summed = allp(*args) | addall
    factor = summed | cur(mul, b=10)

    return (x & factor) | add


@mark.macro
def test_unary_macro_simple():

    # Define source
    source: variable[float] = variable()

    # Define first pipe which ends with macro
    pipe = source | cur(add, b=-2) | f

    # Define ground truth function
    def gt(x):
        x = x - 2
        return x * 2 + (x + 1)

    # Subscribe observer
    out = elicited()
    obs = pipe.subscribe(**out)

    # Check that pipe_base is called once, and confirm outputs
    for val in range(100):

        source.set(val)
        assert out.val == gt(val) and not out.done


@mark.macro
def test_unary_macro():

    # Define source
    source: variable[float] = variable()

    # Define first pipe which ends with macro
    pipe = source | cur(add, b=-2) | f | cur(add, b=3)

    # Define ground truth function
    def gt(x):
        x = x - 2
        return x * 2 + (x + 1) + 3

    # Subscribe observer
    out = elicited()
    obs = pipe.subscribe(**out)

    # Check that pipe_base is called once, and confirm outputs
    for val in range(100):

        source.set(val)
        assert out.val == gt(val) and not out.done


@mark.macro
def test_binary_macro_simple():

    # Define source
    source: variable[float] = variable()

    # Define first pipes to get x and y
    pipe_x = source | cur(add, b=-2)
    pipe_y = source | cur(mul, b=2)

    # Define pipe ending with macro
    pipe = (pipe_x & pipe_y) | g

    # Define ground truth function
    def gt(a):
        x = a - 2
        y = a * 2
        return x * 2 + (y + 1) ** 2

    # Subscribe observer
    out = elicited()
    obs = pipe.subscribe(**out)

    # Check that pipe_base is called once, and confirm outputs
    for val in range(100):

        source.set(val)
        assert out.val == gt(val) and not out.done


@mark.macro
def test_binary_macro_with_currying():

    # Define source
    source: variable[float] = variable()

    # Define first pipes to get x and y
    pipe_x = source | cur(add, b=-2)
    pipe_y = source | cur(mul, b=2)

    # Define pipe ending with macro
    pipe = pipe_x | cur(g, y=pipe_y)

    # Define ground truth function
    def gt(a):
        x = a - 2
        y = a * 2
        return x * 2 + (y + 1) ** 2

    # Subscribe observer
    out = elicited()
    obs = pipe.subscribe(**out)

    # Check that pipe_base is called once, and confirm outputs
    for val in range(100):

        source.set(val)
        assert out.val == gt(val) and not out.done


@mark.macro
def test_binary_macro():

    # Define source
    source: variable[float] = variable()

    # Define first pipes to get x and y
    pipe_x = source | cur(add, b=-2)
    pipe_y = source | cur(mul, b=2)

    # Define pipe ending with macro
    pipe = (pipe_x & pipe_y) | g | cur(add, b=3)

    # Define ground truth function
    def gt(a):
        x = a - 2
        y = a * 2
        return x * 2 + (y + 1) ** 2 + 3

    # Subscribe observer
    out = elicited()
    obs = pipe.subscribe(**out)

    # Check that pipe_base is called once, and confirm outputs
    for val in range(100):

        source.set(val)
        assert out.val == gt(val) and not out.done


@mark.macro
def test_varargs_macro():

    # Define source
    source: variable[float] = variable()

    # Define first pipes to get x and y
    pipe_x = source | cur(add, b=-2)
    pipes_varargs = [(source | cur(mul, b=i)) for i in range(10)]

    # Define ground truth function
    def gt(a, varargs_len):
        x = a - 2
        varargs = [a * i for i in range(varargs_len)]
        return (2 * x + sum(varargs) * 10) + 3

    # Loop through different lengths of varargs
    for val, varargs_len in zip(range(10), [4, 6, 2]):

        # Define pipe ending with macro
        pipe = allp(pipe_x, *pipes_varargs[:varargs_len]) | h | cur(add, b=3)

        # Subscribe observer
        out = elicited()
        obs = pipe.subscribe(**out)

        source.set(val)
        assert out.val == gt(val, varargs_len) and not out.done


@mark.macro
def test_compile_unary():

    # Compile macro
    f_compiled = compile(f)

    # Define ground truth
    def gt(x):
        return (x * 2) + x + 1

    # Check eager function output
    for val in range(100):
        assert f_compiled(val) == gt(val)


@mark.macro
def test_compile_binary():

    # Compile macro
    g_compiled = compile(g)

    # Define ground truth
    def gt(x, y):
        return 2 * x + (y + 1) ** 2

    # Check eager function output
    for val_x, val_y in zip(range(100), range(100, 200)):
        assert g_compiled(val_x, val_y) == gt(val_x, val_y)
