from pytest import mark
from reactpy.core import decorators
from reactpy.rxow.currying import cur
from reactpy.rxow.vario import elicited, variable
from rx import operators as op


def add1(x):
    return x + 1


def sub(x, y):
    return x - y


def mul2(x):
    return x * 2


@decorators.filter_pipe
def gt(x, t):
    return x > 0


@mark.piping
def test_simple_pipe_w_currying():
    """
    This test checks that if a pipe is forked, the first shared part is called only once
    """

    # Define source
    source = variable()

    # Construct initial shared pipe
    pipe = source | mul2 | cur(sub, y=1)

    # Create observers

    out = elicited()
    obs = pipe.subscribe(**out)

    # Check that pipe_base is called once, and confirm outputs
    source.set(2)

    assert out.val == 3 and not out.done


@mark.piping
def test_simple_filter_with_cur():
    """
    This function builds a basic pipe using the piping operator and checks that its behaviour mimics that of a standard pipe
    """

    # Define source
    source = variable()

    # Construct ground truth pipe
    pipe_gt = source.pipe(op.map(add1), op.map(mul2), op.filter(cur(gt, t=10)))

    # Construct pipe using pipe operator
    pipe = source | add1 | mul2 | cur(gt, t=10)

    # Create observers

    out_gt = elicited()
    obs_gt = pipe_gt.subscribe(**out_gt)

    out = elicited()
    obs = pipe.subscribe(**out)

    # Compare pipes

    inputs = 2, 5, 1, 6

    for i in inputs:

        # Send input upstream
        source.set(i)

        # Compare outputs of pipes
        assert out_gt.val == out.val, f"Value mismatch with ground truth for input {i}"
        assert (
            out_gt.done == out.done
        ), f"Completed mismatch with ground truth for input {i}"

    source.close()
    assert out.done and out_gt.done, f"Pipe did not complete"
