from pytest import mark
from reactpy.core import decorators
from reactpy.rxow.vario import elicited, variable
from rx import operators as op


def add1(x: float):
    return x + 1


def sub1(x: float):
    return x - 1


def mul2(x: float):
    return x * 2


def div2(x: float):
    return x / 2


@decorators.filter_pipe
def gt10(x: float):
    return x > 0


class Callonce:
    """
    This class mimics an identity function that can be called only once. If it is called a second time an exception is thrown
    """

    def __init__(self):
        self.__called = False

    def __call__(self, x):

        if self.__called:
            raise Exception(f"Function was called twice")

        self.__called = True
        return x


@mark.piping
def test_sanitization():
    """
    This test checks that if the wrong types are used for the pipe and fork operator then a TypeError exception will be thrown
    """

    # Try piping with non-callable
    try:
        variable() | add1 | 2 | mul2
        raise Exception()
    except TypeError:
        pass
    else:
        raise Exception()

    # Try forking with non-callable
    try:
        # TODO check why if you pipe this and then do `>> 2` it does not throw an Exception from the __rshift__ operator
        variable() >> 2
        raise Exception()
    except TypeError:
        pass
    else:
        raise Exception()


@mark.piping
def test_basic_piping():
    """
    This function builds a basic pipe using the piping operator and checks that its behaviour mimics that of a standard pipe
    """

    # Define source
    source: variable[float] = variable()

    # Construct ground truth pipe
    pipe_gt = source.pipe(op.map(add1), op.map(mul2), op.filter(gt10))

    # Construct pipe using pipe operator
    pipe = source | add1 | mul2 | gt10

    # Create observers

    out_gt = elicited()
    obs_gt = pipe_gt.subscribe(**out_gt)

    out = elicited()
    obs = pipe.subscribe(**out)

    # Compare pipes

    inputs = 2, 5, 1, 6

    for i in inputs:

        # Send input upstream
        source.set(i)

        # Compare outputs of pipes
        assert out_gt.val == out.val, f"Value mismatch with ground truth for input {i}"
        assert (
            out_gt.done == out.done
        ), f"Completed mismatch with ground truth for input {i}"

    source.close()
    assert out.done and out_gt.done, f"Pipe did not complete"


@mark.piping
def test_fork_callonce():
    """
    This test checks that if a pipe is forked, the first shared part is called only once
    """

    # Define source
    source: variable[float] = variable()

    # Construct initial shared pipe
    pipe_base = source | mul2 | Callonce() | add1

    # Construct two separate forks
    pipe1 = pipe_base >> div2 | sub1
    pipe2 = pipe_base >> sub1 | div2

    # Create observers

    out1 = elicited()
    obs1 = pipe1.subscribe(**out1)

    out2 = elicited()
    obs2 = pipe2.subscribe(**out2)

    # Check that pipe_base is called once, and confirm outputs
    source.set(2)

    assert out1.val == 1.5 and not out1.done
    assert out2.val == 2 and not out2.done


@mark.piping
def test_observer_decorator():
    """
    This test checks using the observer decorator to subscribe an observer
    """

    # Create dict to test that observer is called
    has_been_called = dict()

    # Create function to be used as observer
    def obs(x):
        if (x % 2) == 0:
            has_been_called["n"] = has_been_called.get("n", 0) + 1

    # Define source
    source: variable[float] = variable()

    # Construct pipe and subscribe observer
    pipe = source | mul2 | decorators.observer(obs)

    # Check that obs is called and that the output of the final pipe is correct
    source.set(2)
    assert has_been_called.get("n", 0) == 1

    source.set(3)
    assert has_been_called.get("n", 0) == 2


@mark.piping
def test_observer_decorator_continued():
    """
    This test checks using the observer decorator to subscribe an observer, and then continuoing to pipe
    """

    # Create dict to test that observer is called
    has_been_called = dict()

    # Create function to be used as observer
    def obs(x):
        if (x % 2) == 0:
            has_been_called["n"] = has_been_called.get("n", 0) + 1

    # Define source
    source: variable[float] = variable()

    # Construct pipe and subscribe observer
    pipe = source | mul2 | decorators.observer(obs) | add1

    # Create observer for final output
    out = elicited()
    pipe.subscribe(**out)

    # Check that obs is called and that the output of the final pipe is correct
    source.set(2)
    assert has_been_called.get("n", 0) == 1
    assert out.val == 5 and not out.done

    source.set(3)
    assert has_been_called.get("n", 0) == 2
    assert out.val == 7 and not out.done


@mark.piping
def test_pipe_into_var():
    """
    This test checks weather piping into a variable works
    """

    # Create variables
    source: variable[float] = variable()
    x: variable[float] = variable()

    # Create pipes
    pipe_main = source | mul2 | x.obs | add1
    pipe_side = x | mul2

    # Create observers for pipes

    out = elicited()
    pipe_main.subscribe(**out)

    out_side = elicited()
    pipe_side.subscribe(**out_side)

    # Run pipes and check that `x` was set
    source.set(2)
    assert out.val == 5 and not out.done
    assert out_side.val == 8 and not out_side.done

    source.set(3)
    assert out.val == 7 and not out.done
    assert out_side.val == 12 and not out_side.done
