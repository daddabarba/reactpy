from pytest import fixture, mark
from reactpy.core.base_types import Vals
from reactpy.rxow.vario import elicited, variable
from rx import operators as op
from rx import pipe

p1double = pipe(
    op.map(lambda x: x + 1),
    op.map(lambda x: x * 2),
    op.filter(lambda x: x > 10),
)


@mark.piping
def test_mutability():
    """
    Tests immutability of variable class' attributes
    """

    # Create variable
    source = variable()

    # Try to mutate the object
    try:
        source._observers = 2
        raise Exception()  # Should never get here, should throw exception before
    except AttributeError:
        pass
    else:
        raise Exception()


@mark.piping
def test_simple_stream():
    """
    This test creates a simple pipe and tests that the variable class works for it
    """

    # Generate result object
    result = elicited()

    # Create source
    source = variable()

    # Create pipe
    source.pipe(p1double).subscribe(**result)

    # Run pipe and compare outputs
    source.set(2)
    assert result.val is Vals.empty and not result.done

    source.set(5)
    assert result.val == 12 and not result.done

    source.set(1)
    assert result.val == 12 and not result.done

    source.set(6)
    assert result.val == 14 and not result.done

    source.close()
    assert result.val == 14 and result.done


@mark.piping
def test_simple_stream_with():
    """
    This test creates a simple pipe and tests that the variable class works for it, using the with statement with the variable class
    """

    # Generate result object
    result = elicited()

    # Create source
    with variable() as source:

        # Create pipe
        source.pipe(p1double).subscribe(**result)

        # Run pipe and compare outputs
        source.set(2)
        assert result.val is Vals.empty and not result.done

        source.set(5)
        assert result.val == 12 and not result.done

        source.set(1)
        assert result.val == 12 and not result.done

        source.set(6)
        assert result.val == 14 and not result.done

    assert result.val == 14 and result.done


@mark.piping
def test_multihead_simple():

    # Create result objects
    result1 = elicited()
    result2 = elicited()

    # Create single source
    with variable() as source:

        # Create shared pipe
        pipeline = source.pipe(p1double)

        # Create observers
        pipeline.subscribe(**result1)

        pipeline.subscribe(**result2)

        # Run pipe and compare outputs
        source.set(2)
        assert result1.val is Vals.empty and not result1.done
        assert result2.val is Vals.empty and not result2.done

        source.set(5)
        assert result1.val == 12 and not result1.done
        assert result2.val == 12 and not result2.done

        source.set(1)
        assert result1.val == 12 and not result1.done
        assert result2.val == 12 and not result2.done

        source.set(6)
        assert result1.val == 14 and not result1.done
        assert result2.val == 14 and not result2.done

    assert result1.val == 14 and result1.done
    assert result2.val == 14 and result2.done


@mark.piping
def test_multihead_heterogeneous():

    # Create result objects
    result1 = elicited()
    result2 = elicited()

    # Create single source
    with variable() as source:

        # Create shared pipe
        pipeline1 = source.pipe(op.map(lambda x: x + 1))
        pipeline2 = pipeline1.pipe(op.map(lambda x: x * 2), op.filter(lambda x: x > 10))

        # Create observers
        pipeline1.subscribe(**result1)

        pipeline2.subscribe(**result2)

        # Run pipe and compare outputs
        source.set(2)
        assert result1.val == 3 and not result1.done
        assert result2.val is Vals.empty and not result2.done

        source.set(5)
        assert result1.val == 6 and not result1.done
        assert result2.val == 12 and not result2.done

        source.set(1)
        assert result1.val == 2 and not result1.done
        assert result2.val == 12 and not result2.done

        source.set(6)
        assert result1.val == 7 and not result1.done
        assert result2.val == 14 and not result2.done

    assert result1.val == 7 and result1.done
    assert result2.val == 14 and result2.done


@mark.piping
def test_observable_interface():
    """
    This test creates a simple pipe and tests that the variable class works for it. This test does not use `rx.create`
    """

    # Generate result object
    result = elicited()

    # Create source
    source = variable()

    # Create pipe
    source.pipe(p1double).subscribe(**result)

    # Run pipe and compare outputs
    source.set(2)
    assert result.val is Vals.empty and not result.done

    source.set(5)
    assert result.val == 12 and not result.done

    source.set(1)
    assert result.val == 12 and not result.done

    source.set(6)
    assert result.val == 14 and not result.done

    source.close()
    assert result.val == 14 and result.done
