# ReactPy

Here is a library to present some syntactic sugar and some extra functionality build on top of the [RxPy](https://github.com/ReactiveX/RxPY) library. Through the functions and classes provided in this library one should be able to construct (multi-threaded) data-flow diagrams, to code in a reactive functional manner in python.

## Testing

To run the tests on this repo, first install [tox](https://pypi.org/project/tox/) with 

```
pip install tox
```

Then, from the root directory of the repo, simply run

```
python -m tox
```

which will run the unit-tests and produce a coverage report of the code.

## Usage

This library should allow you to use normal python functions in dataflows with as little syntax as possible.

### Basic I/O

#### Input

The input of each data-flow can be any observable from the **RxPy** package. However, these often require the stream of inputs to be pre-defined (either a finite list, or an infinite loop), hindering you from running other instructions on the main thread between sending two values up-stream (passing inputs to the data-flow diagram).

For this purpose the `reactpy.core.vario.variable` class has been provided. Simply construct a variable by

```
source = variable()
```

This variable can be used as an *RxPy Observable*, but it won't send value up-stream (through the pipe). Instead, whenver the method `set` is called, it will notify its observers. If multiple values are passed to `set` the variable will send them up-stream one after the other (similar to `rx.of`). Here is an example

```
from reactpy.core.vario import variable
from rx import operators as op

# Define input
source = variable()

# Construct pipe
pipe = source.pipe(op.map(lambda x: x+1))

# Subscribe Observers
pipe.subscribe(lambda x: print(f"Observer 1: {x}"))
pipe.subscribe(lambda x: print(f"Observer 2: {x}"))

# Send value(s) upstream
source.set(2)
# now `Observer 1: 3` will be printed
# now `Observer 2: 3` will be printed

source.set(3)
# now `Observer 1: 4` will be printed
# now `Observer 2: 4` will be printed
```

The `variable` object also has a method `close`. This takes no arguments and when called will call `on_complete` of its observers. The variable object can be used with the `with` notation as well, where the `__exit__` method will close the variable (calling `on_complete` for its observers). Below an example

```
from reactpy.core.vario import variable
from rx import operators as op

# Define input
with variable() as source:

    # Construct pipe
    pipe = source.pipe(op.map(lambda x: x+1))

    # Subscribe Observers
    pipe.subscribe(on_next=lambda x: print(f"Observer 1: {x}"), on_completed=lambda : print("Done 1"))
    pipe.subscribe(on_next=lambda x: print(f"Observer 2: {x}"), on_completed=lambda : print("Done 2"))

    # Send value(s) upstream
    source.set(2)
    # now `Observer 1: 3` will be printed
    # now `Observer 2: 3` will be printed

    source.set(3)
    # now `Observer 1: 4` will be printed
    # now `Observer 2: 4` will be printed

# now 'Done 1' will be printed
# now 'Done 2' will be printed
```

#### Output

Since *RxPy Observers* have callbacks only, an additional class is provided to easily store and retrieve the output of data-flow diagrams (pipes) in a datastructure. This is the `reactpy.core.vario.elicited` class. Simply create an `elicited` type object, and unpack it when passing it to the `subscribe` method. When `on_next` is called, the `elicited` object will store the passed value in the field `val`. When `on_complete` is called, the `elicited` object will store `True` in the field `done`.

### Piping

You can create pipes using simply the piping operator, starting from a `reactpy.core.vario.variable` object. By default, if a function is piped, then `rx.operators.map` is used. `reactpy.core.pipedec` provied decorators for functions to specify what  *RxPy* operator to use. For isntance, if a function is piped which is decorated with `reactpy.core.pipedec.filter_pipe`, then when piped `rx.operators.filter` will be used (that is, the function is considereded a filter in the pipe). Below an example.

```
from reactpy.core.vario import variable
from reactpy.core import pipedec

def add1(x):
    return x+1

def mul2(x):
    return x*2

@pipedec.filter_pipe
def gt10(x):
    return x>10

# Define input
source = variable()

# Create pipe
pipe = source | add1 | mul2 | gt10

# Subscribe observer
pipe.subscribe(print)

# Send inputs
source.set(2)
# This will output nothing since (2+1)*2 = 6 < 10
source.set(6)
# now `14` will be printed
```

### Forking

One addition to the *RxPy* library is that isntead of defining pipelines, *reactpy* allows you to define diagrams. One pipe can be forked into multiple branching pipes, with the advantage that the pipe before the forking is run once for each value passed up-stream (instead of once per value and per branch, as *RxPy* would do). An example can be found in the code below

```
from reactpy.core.vario import variable

def add1(x):
    return x + 1

def sub1(x):
    return x - 1

def mul2(x):
    return x * 2

def div2(x):
    return x / 2

class Callonce:
    """
    This class mimics an identity function that can be called only once. If it is called a second time an exception is thrown
    """
    def __init__(self):
        self.__called = False

    def __call__(self, x):

        if self.__called:
            raise Exception(f"Function was called twice")

        self.__called = True
        return x


# Define source
source = variable()

# Construct initial shared pipe
pipe_base = source | mul2 | Callonce() | add1

# Construct two separate forks
pipe1 = pipe_base >> div2 | sub1
pipe2 = pipe_base >> sub1 | div2

# Subscribe observers

pipe1.subscribe(lambda x: print(f"Fork 1: {x}"))
pipe2.subscribe(lambda x: print(f"Fork 2: {x}"))

# Check that pipe_base is called once, and confirm outputs
source.set(2)

# now `1.5` will be printed
# now `2.0` will be printed
```

Note that this will not throw an exeption, meaning that the operations of `pipe_base` have been performed only once.

Moreover, after the forking operator is used (`>>`) a different pipe is constructed, this means that the operations after forking can be run on a different thread.

### Concatenating

Sometimes functions in the pipe may require multiple inputs. For this we have provided two means of concatenation. 

#### Conjoint

The first one is *conjoint* concatenation, performed with the operator `&`. This will pass the outputs of multiple pipes as positional arguments to the function piped after the cocnatenation. It is called conjoint as it will pass a new value up-stream (for each of the concatenated pipes) only when **all** concatenated pipes have provided a new value. Below an example

```
from reactpy.core.vario import variable

def add1(x):
    return x + 1

def mul2(x):
    return x * 2

def sum2(x, y):
    return x + y

# Create two variables
source1 = variable()
source2 = variable()

# Create two pipes
pipe1 = source1 | add1
pipe2 = source2 | mul2

# Concatenate pipes
pipe_cat = (pipe1 & pipe2) | sum2

# Create observer
pipe_cat.subscribe(print)

# Check output
source1.set(2)
source2.set(3)

# now `9` will be printed

source1.set(10)
# now nothing is printed
source2.set(2)
# now `15` will be printed
```

#### Disjoint

Sometimes one wants to update the output of the diagram every time a new value is provided in a concatenation, without needing to wait for others to be set. For this purposes we added the disjoint concatenation operator `+`. This, much like the conjoint operator, will pass the output of the concatenated pipes as positional arguments to the following function in the pipe. However, a new output is provided every time a flag pipe is set to an arbitrary value, allowing you to compute a new output every time **any** of the concatenated pipes have provided a new value. Below an example

```
from reactpy.core.vario import variable

def add1(x):
    return x + 1

def mul2(x):
    return x * 2

def sum2(x, y):
    return x + y

# Create two variables
source1 = variable()
source2 = variable()

flag = variable()

# Create two pipes
pipe1 = source1 | add1
pipe2 = source2 | mul2

# Concatenate pipes
pipe_cat = (flag + pipe1 + pipe2) | sum2

# Create observer
pipe_cat.subscribe(print)

# Check output
source1.set(2)
flag.set(True)
# now nothing is printed
source2.set(3)
# now nothing is printed

flag.set(True)
# now `9` will be printed

source1.set(10)
# now nothing is printed
flag.set(True)
# now `17` will be printed

source2.set(2)
# now nothing is printed
flag.set(True)
# now `15` will be printed
```

Note that even if the flag sends a value up-stream, nothing will be forwarded until each of the pipes have provided at least one value. This will of course hold only the firs time. Moreover, while here the `flag` sends `True` up-stream, this can be any value, as the object that `flag` is set to will be ignored as long as one is passed. In fact, while 3 observables are concatenated (`flag`, `pipe1`, and `pipe2`), the following function in the pipeline (`sum2`) receives only the output of `pipe1` and `pipe2`, as the flag is ignored.

### Currying

A re-implementation of `functools.partial` is provided, to be able to perform currying also by keyword argument. Currying can be performed on functions before piping them to generate temporary functions with a smaller arity. Below an example

```
from reactpy.core.vario import variable
from reactpy.core.currying import cur

def mul2(x):
    return x * 2

def sub2(x, y):
    return x - y

# Create variable
source = variable()

# Create pipe (use currying instead of defining a function to just add 1 to an input)
pipe = source | cur(sub2, y=1) | mul2

# Create observer
pipe.subscribe(print)

# Check output
source.set(6)
# now `10` will be printed
```
